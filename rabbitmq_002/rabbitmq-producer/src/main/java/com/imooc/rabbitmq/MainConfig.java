package com.imooc.rabbitmq;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.imooc.rabbitmq.*"})
public class MainConfig {

}
