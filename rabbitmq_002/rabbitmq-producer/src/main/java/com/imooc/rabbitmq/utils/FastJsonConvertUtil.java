package com.imooc.rabbitmq.utils;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.imooc.rabbitmq.entity.Order;

public class FastJsonConvertUtil {
	
	private final static String featuresWithNullValue = "" ;
	
	/**
	 * 
	 * @param message
	 * @param class1
	 * @return 
	 * @return
	 */
	public static <T> List<T> convertJSONToArray(List<JSONObject> data, Class<T> clazz) {
		try {
			List<T> t = new ArrayList<T>();
			for (JSONObject jsonObject : data) {
				t.add(convertJSONToObject(jsonObject, clazz));
			}
			return t ;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 将对象转为JSON字符串
	 * @param obj 任意对象
	 * @return JSON字符串
	 */
	public static <T> T convertJSONToObject(JSONObject jsonObject, Class<T> clazz) {
		 try {
				T t = JSONObject.toJavaObject(jsonObject, clazz) ;
				return t ;
			}catch (Exception e) {
				e.printStackTrace();
				return null;
			}
	}
	
	/**
	 * 将对象转为JSON字符串
	 * @param obj 任意对象
	 * @return JSON字符串
	 */
	public static <T> T convertJSONToObject(String str, Class<T> clazz) {
		try {
			T t = JSONObject.parseObject(str, clazz);
			return t ;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 将对象转为JSON字符串
	 * @param obj 任意对象
	 * @return JSON字符串
	 */
	public static String convertObjectToJSON(Object obj) {
		try {
			String text = JSON.toJSONString(obj);
			return text ;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 将任意对象转成JSONObject对象
	 * @param obj 任意对象
	 * @return JSONObject对象
	 */
	public static JSONObject convertObjectToJSONObject(Object obj) {
		try {
			JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSON(obj).toString());
			return jsonObject;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static String convertObjectToJSONWithNullValue(Object obj) {
		return null;
	}

}
