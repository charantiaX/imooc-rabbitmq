package com.imooc.rabbitmq;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.imooc.rabbitmq.entity.Order;
import com.imooc.rabbitmq.producer.RabbitSender;
import com.imooc.rabbitmq.service.OrderService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitmqProducterApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Autowired
	private RabbitSender orderSender ;

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat() ;
	
	@Test
	public void testSend1() throws Exception {
		Map<String, Object> properties = new HashMap<>() ;
		properties.put("number", "123");
		properties.put("send_time",simpleDateFormat.format(new Date()));
		orderSender.send("Hello RabbitMQ For Spring Boot !", properties);
	}
	
	@Autowired
	private OrderService orderService ;
	
	@Test
	public void testSend() throws Exception {
		Order order = new Order() ;
		order.setId("201809030000000001");
		order.setName("测试订单1");
		order.setMessageId(System.currentTimeMillis() + "$" + UUID.randomUUID().toString());
		orderService.createOrder(order);
	}
	
}
